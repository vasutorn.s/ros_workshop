#!/usr/bin/env python
import rospy

if __name__ == "__main__":
    rospy.init_node("test_log",log_level=rospy.DEBUG)

    rate = rospy.Rate(10) # 10 Hz

    while not rospy.is_shutdown():
        rospy.logdebug("*** debug ***")
        rospy.loginfo("*** info ***")
        rospy.logwarn("*** warning ***")
        rospy.logerr("*** error ***")
        rospy.logfatal("*** fatal ***")
        rate.sleep()