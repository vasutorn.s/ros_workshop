#!/usr/bin/env python
import rospy
from std_msgs.msg import String

def talker():
    rospy.init_node("talker",anonymous=True)
    pub = rospy.Publisher('chatter', String, queue_size=10)
    
    rate = rospy.Rate(10) # 10Hz

    while not rospy.is_shutdown():
        hello_str = "hello world"
        str_msg = String()
        str_msg.data = hello_str
        pub.publish(str_msg)
        rospy.loginfo(hello_str)

        rate.sleep()

if __name__ == "__main__":
    try:
        talker()
    except rospy.ROSInterruptException:
        pass